Please complete the following:
Create a custom block that does the following:

* Renders its own template.
* Displays a simple Bootstrap 4 accordion with lorem ipsum text.
* Create a static route using a Controller, the Controller should do the following:
* Have a path /dashboard that is only accessible to authenticated users.
* Renders the block in Step 1
* The Bootstrap 4 Library should be attached to this Controller only.
* Create an EventSubscriber that redirects a user when they login to the path outlined in #2
* Bonus points if your code passes all PHPStan and Drupal Coding Standard tests.