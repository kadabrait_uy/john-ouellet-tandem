<?php

namespace Drupal\tandem\Controller;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultController for Tandem module.
 */
final class DefaultController extends ControllerBase {

  /**
   * Drupal\Core\Session\AccountInterface definition.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Drupal\Core\Block\BlockManagerInterface definition.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * Constructs a new DefaultController object.
   */
  public function __construct(AccountInterface $current_user, BlockManagerInterface $block_manager) {
    $this->currentUser = $current_user;
    $this->blockManager = $block_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('plugin.manager.block')
    );
  }

  /**
   * Render Tandem block.
   *
   * @return array
   *   Block.
   */
  public function dashboard() {
    $config        = [];
    $plugin_block  = $this->blockManager->createInstance('tandem_block', $config);
    $access_result = $plugin_block->access($this->currentUser);
    if (is_object($access_result) &&
        $access_result->isForbidden() ||
        is_bool($access_result)
        && !$access_result) {
      return [];
    }
    return $plugin_block->build();
  }

}
