<?php

namespace Drupal\tandem\EventSubscriber;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\tandem\Event\UserLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class UserLoginSubscriber for custom events.
 *
 * @package Drupal\tandem\EventSubscriber
 */
class UserLoginSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Session\AccountInterface definition.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a new UserLoginSubscriber object.
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      UserLoginEvent::EVENT_NAME => 'onUserLogin',
    ];
  }

  /**
   * Subscribe to the user login event dispatched.
   *
   * @param \Drupal\tandem\Event\UserLoginEvent $event
   *   Custom UserLoginEvent object.
   */
  public function onUserLogin(UserLoginEvent $event) {
    if ($this->currentUser->id()) {
      $response = new RedirectResponse(
        Url::fromRoute('tandem.dashboard')->toString(), 301
      );
      $response->send();
    }
  }

}
